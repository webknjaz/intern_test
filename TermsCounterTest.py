from TermsCounter import TermsCounter

import sys

ESC_CHAR = chr(27)
OK_STR = "%(esc)c[32mOK%(esc)c[0m" % {'esc': ESC_CHAR}
FAIL_STR = "%(esc)c[31mFAIL%(esc)c[0m" % {'esc': ESC_CHAR}

def test_terms_counter(name, text, terms, answer):
    """
    Runs compute_occurrences_count twice for the same instance of TermsCounter
    Then checks if the obtained results are correct
    """
    if len(terms) != len(answer):
        raise AssertionError
    print "%s:" % name
    terms_counter = TermsCounter(terms)
    result1 = terms_counter.compute_occurrences_count(text)
    result2 = terms_counter.compute_occurrences_count(text)
    test_ok = False
    if len(result1) != len(answer):
        print "    The size of the result vector is incorrect:" \
              " %d instead of %d" % (len(result1), len(answer))
    elif result1 != result2:
        print "    The TermsCounter returns different results for" \
              " the same text"
    elif result1 != answer:
        for i in range(len(answer)):
            if result1[i] != answer[i]:
                print "    Occurrences count for \"%s\" is incorrect:" \
                      " %d instead of %d" % (terms[i], result1[i], answer[i])
    else:
        test_ok = True
    print "    %s" % (OK_STR if test_ok else FAIL_STR)
    return test_ok


# Run tests
ok = True

# NOTE: This set of tests is NOT complete.
# We suggest you to extend it.

ok = test_terms_counter(
    name="Test1",
    text="This is an example term",
    terms=("is", "example term"),
    answer=(1, 1)
) and ok

ok = test_terms_counter(
    name="Test2",
    text="This is not a \n\n term.",
    terms=("a term",),
    answer=(0,)
) and ok

ok = test_terms_counter(
    name="Test3",
    text="This this this this",
    terms=("this this",),
    answer=(2,)
) and ok

print "\nOverall:\n    %s" % (OK_STR if ok else FAIL_STR)

sys.exit(0 if ok else 1)
