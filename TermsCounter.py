"""
A TermsCounter class holds a large number of terms and computes the number of
occurrences of each term in a large ASCII text. A typical use case for this
class is to create one instance of it for a large (but fixed) list of terms and
to use this instance to compute the occurrences of these terms in multiple
texts.

A term is a sequence of words (made of alphanumeric characters, as defined by
the isalpha() function) separated by spaces.

A text is a sequence of words separated by non-alphanumeric characters.

A term occurs in a text if all of its words are found in the text in the same
sequence separated by any number of space-like characters (as defined by the
isspace() function), but not paragraph breaks (a newline followed by another
newline, "\n\n"). The comparison of words is case-insensitive.

If several different terms overlap in a text, an occurrence is counted for all
of the overlapping terms. However, a term that overlaps with itself should not
be double-counted.

For example, the term "Computer science" occurs in a text "computer science",
"computer   science" and "computer\nscience" but neither in "computer, science"
nor in "computer\n\nscience". In this example, your implementation of
TermsCounter.compute_occurrences_count() should return 1 occurrence for the
former three examples and zero occurrences for the latter two examples.

According to the use case described above, your algorithm should run in a time
that depends linearly on the total size of all terms N and the size of the text
L: T = O(N + L). This requirement is mandatory. If you are not able to come up
with such algorithm during the first 30 minutes, please ask us for a hint.

You should also implement a tool that, given a large list of terms (in a text
file, each term on its own line) and a folder, finds N most frequently
occurring terms in all files from that folder (and its subfolders,
recursively).

You are allowed to use standard Python libraries.

Please keep in mind the following requirements, ordered according to their
priority (from the most to the least important):

- Your code should run on pure python 2.7+ installation, without any additional
  libraries installed.

- Your code should be correct. We expect you will test your code. Crashes,
  infinite loops, wrong results, etc. are considered major problems.

- Your code should satisfy the performance requirement given above.

- Your code should use as small amount of memory as possible in the use case
  described above (but the performance is more important then memory usage).

- Your code should contain unit tests. The line and branch coverage achieved by
  your tests will influence your grade.

- Your code should be clean and readable according to the PEP8 guidlines. We
  use pep8 and pylint to check the code.

- Your code should not use any deprecated features of Python 2.7.
"""

import re


class TermsCounter(object):
    """
    The main class
    """

    _start_words = {}
    _words_pattern = re.compile(r'\s+|\n{1}')
    _terms_list = {}

    def __init__(self, terms_list):
        """
        Constructor. Creates new TermsCounter instance from a list of terms.
        """

        assert isinstance(terms_list, tuple), "Please pass tuple instance."

        for term in terms_list:
            self._terms_list[term] = tuple(term.split(" "))

            for word in self._terms_list[term]:
                word = word.lower()
                if word not in self._start_words:
                    self._start_words[word] = set()
                self._start_words[word].add(term)

    def get_terms(self):
        """
        Get a list of terms, in the same order as specified
        when constructing the object.
        """
        return tuple([term for term in self._terms_list])

    def compute_occurrences_count(self, text):
        """
        Computes the number of occurrences of each term in a text.

        Returns a list of computed number of occurrences, in the same order as
        specified when constricting the object.

        This function should not change the state of the object.  This ensures
        that one object may be reused to compute occurrences in several texts.
        """

        term_occurrences = {}

        for term in self._terms_list:
            term_occurrences[term] = 0

        text_words = self._words_pattern.split(text)
        skip_words = 0
        for word in text_words:
            if skip_words > 0:
                skip_words -= 1
                continue

            word = word.lower()

            if word in self._start_words:
                possible_terms = self._start_words[word]

                term = self.check_possible_term(possible_terms, text_words)

                if term is not None:
                    term_occurrences[term] += 1
                    skip_words = len(self._terms_list[term]) - 1

        return tuple([term_occurrences[term] for term in term_occurrences])

    def check_possible_term(self, possible_terms, text_words):
        """
        Checks whether one of possible terms starts at current position
        """
        for term in possible_terms:
            definition = self._terms_list[term]

            term_found = True
            for (num, word) in enumerate(definition[1:]):
                if word.lower() != text_words[num + 1].lower():
                    term_found = False
                    break

            if term_found:
                return term

        return None
